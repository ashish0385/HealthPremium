package com.emids.healthpremium;

public class Premium {

	public static float agePremiumAmount(Person person, float basePremium) {

		int personAge = person.getAge();
		//		float bprium = basePremium;
		float finalpremium = basePremium;
		int i = 25;
		boolean genderMale = person.isMale();
		
		if (personAge >= 18) {
			finalpremium = finalpremium + ((finalpremium * 10) / 100);

			if (genderMale) {

				finalpremium += (finalpremium * 2) / 100;

			}
		}

		if (personAge > 25) {

			while (i < personAge) {

				if (i < 40) {
					finalpremium += (finalpremium * 10) / 100;

					if (genderMale) {

						finalpremium += (finalpremium * 2) / 100;

					}
				}

				if (i >= 40) {
					finalpremium += (finalpremium * 20) / 100;

					if (genderMale) {

						finalpremium += (finalpremium * 2) / 100;

					}
				}

				i = i + 5;
			} 

		}

		finalpremium = badHabit(person, finalpremium);
		
		finalpremium = goodHabit(person, finalpremium);

		return finalpremium;
	}

	private static float goodHabit(Person person, float finalpremium) {
		if(person.isHavingGoodHabit()){
			return finalpremium-((finalpremium*3)/100);
		} else {
			return finalpremium;
		}
	}

	private static float badHabit(Person person, float premium) {

		int count =0;
		if(person.isSmoker()) {
			count+=1;
		}
		if(person.isAlchoholic()) {
			count+=1;
		}

		if(person.isDrugAddict()) {
			count+=1;
		}
		System.out.println(count+"P:_"+premium);
		for(int i=0;i<count;i++)
		{
			System.out.println(count+"inP:_"+premium);
			premium=premium+((premium*3)/100);
		}
		return premium;
	}

	public static void main(String[] args) {

		float base_pre=5000;
		try{
			Person person = new Person("Ashish Kumar", 26, 'M');

			person.setAlchoholic(true);
			person.setDrugAddict(true);
			person.setSmoker(true);
			person.setHavingGoodHabit(true);

			float finalpre=agePremiumAmount( person, base_pre);

//			boolean good_habit=true;
//
//			String Smoking="yes";
//			String Alcohol="yes";
//			String Drugs="yes";

//			finalpre=badHabit(Smoking,Alcohol,Drugs,finalpre);

//			if(good_habit) {
//
//				finalpre=finalpre-((finalpre*3)/100);
//			}
			System.out.println("Your age is "+person.getAge()+ "and your premium  will come " +finalpre);
		}catch (Exception e){
			e.printStackTrace();
		}
	}

}
