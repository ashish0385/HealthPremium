package com.emids.healthpremium;

public class InvalidGenderException extends Exception {

	public InvalidGenderException(String msg){
		super(msg);
	}
}
