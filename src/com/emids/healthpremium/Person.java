/**
 * 
 */
package com.emids.healthpremium;

public class Person {
	public Person(String name, int age, char gender) throws InvalidGenderException {
		super();
		this.name = name;
		this.age = age;
		gender=Character.toUpperCase(gender);
		if(gender =='M' || gender=='F')
			this.gender = gender;
		else
			throw new InvalidGenderException("Invalid Gender Value");
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public char getGender() {
		return gender;
	}
	public void setGender(char gender) throws InvalidGenderException{
		gender=Character.toUpperCase(gender);
		if(gender =='M' || gender=='F')
			this.gender = gender;
		else
			throw new InvalidGenderException("Invalid Gender Value");
	}
	public boolean isSmoker() {
		return isSmoker;
	}
	public void setSmoker(boolean isSmoker) {
		this.isSmoker = isSmoker;
	}
	public boolean isAlchoholic() {
		return isAlchoholic;
	}
	public void setAlchoholic(boolean isAlchoholic) {
		this.isAlchoholic = isAlchoholic;
	}
	public boolean isDrugAddict() {
		return isDrugAddict;
	}
	public void setDrugAddict(boolean isDrugAddict) {
		this.isDrugAddict = isDrugAddict;
	}
	public boolean isMale(){
		return (gender=='M' || gender =='m') ? true : false;
	}
	public boolean isHavingGoodHabit() {
		return isHavingGoodHabit;
	}
	public void setHavingGoodHabit(boolean havingGoodHabit){
		this.isHavingGoodHabit = havingGoodHabit;
	}
	private String name;
	private int age;
	private char gender;
	private boolean isSmoker;
	private boolean isAlchoholic;
	private boolean isDrugAddict;
	private boolean isHavingGoodHabit;
	

}
